import { FetchTodosAction, DeleteTodoAction } from "./todos";
//import { FetchBlogs, EditTodosAction } from "./todos";

export enum ActionTypes {
  fetchTodos,
  deleteTodos
}

//export type Action = FetchTodosAction | DeleteTodoAction | FetchBlogs | EditTodosAction
export type Action = FetchTodosAction | DeleteTodoAction;
